import pandas as pd


def add(a, b):
    if isinstance(a, float) and isinstance(b, float):
        return a + b
    elif isinstance(a, float):
        b["value"] += a
        return b
    elif isinstance(b, float):
        a["value"] += b
        return a
    else:
        res = pd.concat([a, b], sort=False)
        # move value column to last
        cols = list(res.columns.values)
        cols.remove("value")
        res = res[cols + ["value"]]
        # aggregate
        group_by = [col for col in res.columns.values[:-1]]
        res[group_by] = res[group_by].fillna("")
        res = res.groupby(group_by, as_index=False).sum()
        return res


def sub(a, b):
    if isinstance(a, float) and isinstance(b, float):
        return a - b
    elif isinstance(a, float):
        b["value"] = a - b["value"]
        return b
    elif isinstance(b, float):
        a["value"] -= b
        return a
    else:
        b["value"] *= -1
        res = pd.concat([a, b], sort=False)
        # move value column to last
        cols = list(res.columns.values)
        cols.remove("value")
        res = res[cols + ["value"]]

        # aggregate
        group_by = [col for col in res.columns.values[:-1]]
        res[group_by] = res[group_by].fillna("")
        res = res.groupby(group_by, as_index=False).sum()
        return res


def mul(a, b):
    if isinstance(a, float) and isinstance(b, float):
        return a * b
    elif isinstance(a, float):
        b["value"] *= a
        return b
    elif isinstance(b, float):
        a["value"] *= b
        return a
    else:
        common_columns = [col for col in a.columns.values[:-1] if col in b]
        if not common_columns:
            return pd.DataFrame({"value": []})
        # calculate
        res = pd.merge(a, b, how="inner", on=common_columns)
        res["value"] = res["value_x"] * res["value_y"]
        res.drop(columns=["value_x", "value_y"], inplace=True)
        return res


def div(a, b):
    if isinstance(a, float) and isinstance(b, float):
        # division by zero??
        return a / b
    elif isinstance(a, float):
        b["value"] = a / b["value"]
        return b
    elif isinstance(b, float):
        a["value"] /= b
        return a
    else:
        common_columns = [col for col in a.columns.values[:-1] if col in b]
        if not common_columns:
            return pd.DataFrame({"value": []})

        # calculate
        res = pd.merge(a, b, how="inner", on=common_columns)
        res["value"] = res["value_x"] / res["value_y"]
        res.drop(columns=["value_x", "value_y"], inplace=True)
        return res
