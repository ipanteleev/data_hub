# Generated from C:/Users/ipanteleev/PycharmProjects/customOperators\evaluate.g4 by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .evaluateParser import evaluateParser
else:
    from evaluateParser import evaluateParser

# This class defines a complete listener for a parse tree produced by evaluateParser.
class evaluateListener(ParseTreeListener):

    # Enter a parse tree produced by evaluateParser#query.
    def enterQuery(self, ctx:evaluateParser.QueryContext):
        pass

    # Exit a parse tree produced by evaluateParser#query.
    def exitQuery(self, ctx:evaluateParser.QueryContext):
        pass


    # Enter a parse tree produced by evaluateParser#expression.
    def enterExpression(self, ctx:evaluateParser.ExpressionContext):
        pass

    # Exit a parse tree produced by evaluateParser#expression.
    def exitExpression(self, ctx:evaluateParser.ExpressionContext):
        pass


    # Enter a parse tree produced by evaluateParser#term.
    def enterTerm(self, ctx:evaluateParser.TermContext):
        pass

    # Exit a parse tree produced by evaluateParser#term.
    def exitTerm(self, ctx:evaluateParser.TermContext):
        pass


    # Enter a parse tree produced by evaluateParser#atom.
    def enterAtom(self, ctx:evaluateParser.AtomContext):
        pass

    # Exit a parse tree produced by evaluateParser#atom.
    def exitAtom(self, ctx:evaluateParser.AtomContext):
        pass


    # Enter a parse tree produced by evaluateParser#variable.
    def enterVariable(self, ctx:evaluateParser.VariableContext):
        pass

    # Exit a parse tree produced by evaluateParser#variable.
    def exitVariable(self, ctx:evaluateParser.VariableContext):
        pass


    # Enter a parse tree produced by evaluateParser#out_driver.
    def enterOut_driver(self, ctx:evaluateParser.Out_driverContext):
        pass

    # Exit a parse tree produced by evaluateParser#out_driver.
    def exitOut_driver(self, ctx:evaluateParser.Out_driverContext):
        pass


    # Enter a parse tree produced by evaluateParser#number.
    def enterNumber(self, ctx:evaluateParser.NumberContext):
        pass

    # Exit a parse tree produced by evaluateParser#number.
    def exitNumber(self, ctx:evaluateParser.NumberContext):
        pass


