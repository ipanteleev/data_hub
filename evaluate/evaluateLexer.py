# Generated from C:/Users/ipanteleev/PycharmProjects/customOperators\evaluate.g4 by ANTLR 4.7.2
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\16")
        buf.write("i\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\3\2\3\2\7\2&\n\2\f")
        buf.write("\2\16\2)\13\2\3\3\3\3\3\3\3\3\3\3\7\3\60\n\3\f\3\16\3")
        buf.write("\63\13\3\3\3\3\3\3\4\5\48\n\4\3\5\3\5\3\5\6\5=\n\5\r\5")
        buf.write("\16\5>\3\6\5\6B\n\6\3\7\3\7\3\b\6\bG\n\b\r\b\16\bH\3\b")
        buf.write("\3\b\6\bM\n\b\r\b\16\bN\5\bQ\n\b\3\t\3\t\3\n\3\n\3\13")
        buf.write("\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3")
        buf.write("\21\6\21d\n\21\r\21\16\21e\3\21\3\21\2\2\22\3\3\5\2\7")
        buf.write("\2\t\4\13\2\r\2\17\5\21\6\23\7\25\b\27\t\31\n\33\13\35")
        buf.write("\f\37\r!\16\3\2\6\7\2\62;C\\aac|\u0412\u0451\6\2C\\aa")
        buf.write("c|\u0412\u0451\4\2..\60\60\5\2\13\f\17\17\"\"\2l\2\3\3")
        buf.write("\2\2\2\2\t\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2")
        buf.write("\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2")
        buf.write("\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\3#\3\2\2\2\5*")
        buf.write("\3\2\2\2\7\67\3\2\2\2\t9\3\2\2\2\13A\3\2\2\2\rC\3\2\2")
        buf.write("\2\17F\3\2\2\2\21R\3\2\2\2\23T\3\2\2\2\25V\3\2\2\2\27")
        buf.write("X\3\2\2\2\31Z\3\2\2\2\33\\\3\2\2\2\35^\3\2\2\2\37`\3\2")
        buf.write("\2\2!c\3\2\2\2#\'\5\5\3\2$&\5\7\4\2%$\3\2\2\2&)\3\2\2")
        buf.write("\2\'%\3\2\2\2\'(\3\2\2\2(\4\3\2\2\2)\'\3\2\2\2*+\7k\2")
        buf.write("\2+,\7p\2\2,-\3\2\2\2-\61\4\63;\2.\60\4\62;\2/.\3\2\2")
        buf.write("\2\60\63\3\2\2\2\61/\3\2\2\2\61\62\3\2\2\2\62\64\3\2\2")
        buf.write("\2\63\61\3\2\2\2\64\65\7a\2\2\65\6\3\2\2\2\668\t\2\2\2")
        buf.write("\67\66\3\2\2\28\b\3\2\2\29<\5\13\6\2:=\5\13\6\2;=\5\r")
        buf.write("\7\2<:\3\2\2\2<;\3\2\2\2=>\3\2\2\2><\3\2\2\2>?\3\2\2\2")
        buf.write("?\n\3\2\2\2@B\t\3\2\2A@\3\2\2\2B\f\3\2\2\2CD\4\62;\2D")
        buf.write("\16\3\2\2\2EG\4\62;\2FE\3\2\2\2GH\3\2\2\2HF\3\2\2\2HI")
        buf.write("\3\2\2\2IP\3\2\2\2JL\t\4\2\2KM\4\62;\2LK\3\2\2\2MN\3\2")
        buf.write("\2\2NL\3\2\2\2NO\3\2\2\2OQ\3\2\2\2PJ\3\2\2\2PQ\3\2\2\2")
        buf.write("Q\20\3\2\2\2RS\7*\2\2S\22\3\2\2\2TU\7+\2\2U\24\3\2\2\2")
        buf.write("VW\7-\2\2W\26\3\2\2\2XY\7/\2\2Y\30\3\2\2\2Z[\7,\2\2[\32")
        buf.write("\3\2\2\2\\]\7\61\2\2]\34\3\2\2\2^_\7?\2\2_\36\3\2\2\2")
        buf.write("`a\7\60\2\2a \3\2\2\2bd\t\5\2\2cb\3\2\2\2de\3\2\2\2ec")
        buf.write("\3\2\2\2ef\3\2\2\2fg\3\2\2\2gh\b\21\2\2h\"\3\2\2\2\r\2")
        buf.write("\'\61\67<>AHNPe\3\b\2\2")
        return buf.getvalue()


class evaluateLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    VARIABLE = 1
    OUT_DRIVER = 2
    NUMBER = 3
    LPAREN = 4
    RPAREN = 5
    PLUS = 6
    MINUS = 7
    TIMES = 8
    DIV = 9
    EQUAL = 10
    POINT = 11
    WS = 12

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'('", "')'", "'+'", "'-'", "'*'", "'/'", "'='", "'.'" ]

    symbolicNames = [ "<INVALID>",
            "VARIABLE", "OUT_DRIVER", "NUMBER", "LPAREN", "RPAREN", "PLUS", 
            "MINUS", "TIMES", "DIV", "EQUAL", "POINT", "WS" ]

    ruleNames = [ "VARIABLE", "VALID_ID_START", "VALID_ID_CHAR", "OUT_DRIVER", 
                  "OUT_LETTER", "OUT_DIGIT", "NUMBER", "LPAREN", "RPAREN", 
                  "PLUS", "MINUS", "TIMES", "DIV", "EQUAL", "POINT", "WS" ]

    grammarFileName = "evaluate.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


