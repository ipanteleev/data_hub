# Generated from C:/Users/ipanteleev/PycharmProjects/customOperators\evaluate.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\16")
        buf.write("\64\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4")
        buf.write("\b\t\b\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\7\3\31\n\3\f\3")
        buf.write("\16\3\34\13\3\3\4\3\4\3\4\7\4!\n\4\f\4\16\4$\13\4\3\5")
        buf.write("\3\5\3\5\3\5\3\5\3\5\5\5,\n\5\3\6\3\6\3\7\3\7\3\b\3\b")
        buf.write("\3\b\2\2\t\2\4\6\b\n\f\16\2\4\3\2\b\t\3\2\n\13\2\60\2")
        buf.write("\20\3\2\2\2\4\25\3\2\2\2\6\35\3\2\2\2\b+\3\2\2\2\n-\3")
        buf.write("\2\2\2\f/\3\2\2\2\16\61\3\2\2\2\20\21\5\f\7\2\21\22\7")
        buf.write("\f\2\2\22\23\5\4\3\2\23\24\7\2\2\3\24\3\3\2\2\2\25\32")
        buf.write("\5\6\4\2\26\27\t\2\2\2\27\31\5\6\4\2\30\26\3\2\2\2\31")
        buf.write("\34\3\2\2\2\32\30\3\2\2\2\32\33\3\2\2\2\33\5\3\2\2\2\34")
        buf.write("\32\3\2\2\2\35\"\5\b\5\2\36\37\t\3\2\2\37!\5\b\5\2 \36")
        buf.write("\3\2\2\2!$\3\2\2\2\" \3\2\2\2\"#\3\2\2\2#\7\3\2\2\2$\"")
        buf.write("\3\2\2\2%,\5\16\b\2&,\5\n\6\2\'(\7\6\2\2()\5\4\3\2)*\7")
        buf.write("\7\2\2*,\3\2\2\2+%\3\2\2\2+&\3\2\2\2+\'\3\2\2\2,\t\3\2")
        buf.write("\2\2-.\7\3\2\2.\13\3\2\2\2/\60\7\4\2\2\60\r\3\2\2\2\61")
        buf.write("\62\7\5\2\2\62\17\3\2\2\2\5\32\"+")
        return buf.getvalue()


class evaluateParser ( Parser ):

    grammarFileName = "evaluate.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "'('", "')'", "'+'", "'-'", "'*'", "'/'", "'='", "'.'" ]

    symbolicNames = [ "<INVALID>", "VARIABLE", "OUT_DRIVER", "NUMBER", "LPAREN", 
                      "RPAREN", "PLUS", "MINUS", "TIMES", "DIV", "EQUAL", 
                      "POINT", "WS" ]

    RULE_query = 0
    RULE_expression = 1
    RULE_term = 2
    RULE_atom = 3
    RULE_variable = 4
    RULE_out_driver = 5
    RULE_number = 6

    ruleNames =  [ "query", "expression", "term", "atom", "variable", "out_driver", 
                   "number" ]

    EOF = Token.EOF
    VARIABLE=1
    OUT_DRIVER=2
    NUMBER=3
    LPAREN=4
    RPAREN=5
    PLUS=6
    MINUS=7
    TIMES=8
    DIV=9
    EQUAL=10
    POINT=11
    WS=12

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class QueryContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def out_driver(self):
            return self.getTypedRuleContext(evaluateParser.Out_driverContext,0)


        def EQUAL(self):
            return self.getToken(evaluateParser.EQUAL, 0)

        def expression(self):
            return self.getTypedRuleContext(evaluateParser.ExpressionContext,0)


        def EOF(self):
            return self.getToken(evaluateParser.EOF, 0)

        def getRuleIndex(self):
            return evaluateParser.RULE_query

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterQuery" ):
                listener.enterQuery(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitQuery" ):
                listener.exitQuery(self)




    def query(self):

        localctx = evaluateParser.QueryContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_query)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 14
            self.out_driver()
            self.state = 15
            self.match(evaluateParser.EQUAL)
            self.state = 16
            self.expression()
            self.state = 17
            self.match(evaluateParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(evaluateParser.TermContext)
            else:
                return self.getTypedRuleContext(evaluateParser.TermContext,i)


        def PLUS(self, i:int=None):
            if i is None:
                return self.getTokens(evaluateParser.PLUS)
            else:
                return self.getToken(evaluateParser.PLUS, i)

        def MINUS(self, i:int=None):
            if i is None:
                return self.getTokens(evaluateParser.MINUS)
            else:
                return self.getToken(evaluateParser.MINUS, i)

        def getRuleIndex(self):
            return evaluateParser.RULE_expression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpression" ):
                listener.enterExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpression" ):
                listener.exitExpression(self)




    def expression(self):

        localctx = evaluateParser.ExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_expression)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 19
            self.term()
            self.state = 24
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==evaluateParser.PLUS or _la==evaluateParser.MINUS:
                self.state = 20
                _la = self._input.LA(1)
                if not(_la==evaluateParser.PLUS or _la==evaluateParser.MINUS):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 21
                self.term()
                self.state = 26
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TermContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def atom(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(evaluateParser.AtomContext)
            else:
                return self.getTypedRuleContext(evaluateParser.AtomContext,i)


        def TIMES(self, i:int=None):
            if i is None:
                return self.getTokens(evaluateParser.TIMES)
            else:
                return self.getToken(evaluateParser.TIMES, i)

        def DIV(self, i:int=None):
            if i is None:
                return self.getTokens(evaluateParser.DIV)
            else:
                return self.getToken(evaluateParser.DIV, i)

        def getRuleIndex(self):
            return evaluateParser.RULE_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm" ):
                listener.enterTerm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm" ):
                listener.exitTerm(self)




    def term(self):

        localctx = evaluateParser.TermContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_term)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 27
            self.atom()
            self.state = 32
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==evaluateParser.TIMES or _la==evaluateParser.DIV:
                self.state = 28
                _la = self._input.LA(1)
                if not(_la==evaluateParser.TIMES or _la==evaluateParser.DIV):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 29
                self.atom()
                self.state = 34
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AtomContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def number(self):
            return self.getTypedRuleContext(evaluateParser.NumberContext,0)


        def variable(self):
            return self.getTypedRuleContext(evaluateParser.VariableContext,0)


        def LPAREN(self):
            return self.getToken(evaluateParser.LPAREN, 0)

        def expression(self):
            return self.getTypedRuleContext(evaluateParser.ExpressionContext,0)


        def RPAREN(self):
            return self.getToken(evaluateParser.RPAREN, 0)

        def getRuleIndex(self):
            return evaluateParser.RULE_atom

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAtom" ):
                listener.enterAtom(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAtom" ):
                listener.exitAtom(self)




    def atom(self):

        localctx = evaluateParser.AtomContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_atom)
        try:
            self.state = 41
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [evaluateParser.NUMBER]:
                self.enterOuterAlt(localctx, 1)
                self.state = 35
                self.number()
                pass
            elif token in [evaluateParser.VARIABLE]:
                self.enterOuterAlt(localctx, 2)
                self.state = 36
                self.variable()
                pass
            elif token in [evaluateParser.LPAREN]:
                self.enterOuterAlt(localctx, 3)
                self.state = 37
                self.match(evaluateParser.LPAREN)
                self.state = 38
                self.expression()
                self.state = 39
                self.match(evaluateParser.RPAREN)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VariableContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VARIABLE(self):
            return self.getToken(evaluateParser.VARIABLE, 0)

        def getRuleIndex(self):
            return evaluateParser.RULE_variable

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVariable" ):
                listener.enterVariable(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVariable" ):
                listener.exitVariable(self)




    def variable(self):

        localctx = evaluateParser.VariableContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_variable)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 43
            self.match(evaluateParser.VARIABLE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Out_driverContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def OUT_DRIVER(self):
            return self.getToken(evaluateParser.OUT_DRIVER, 0)

        def getRuleIndex(self):
            return evaluateParser.RULE_out_driver

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOut_driver" ):
                listener.enterOut_driver(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOut_driver" ):
                listener.exitOut_driver(self)




    def out_driver(self):

        localctx = evaluateParser.Out_driverContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_out_driver)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 45
            self.match(evaluateParser.OUT_DRIVER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NumberContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBER(self):
            return self.getToken(evaluateParser.NUMBER, 0)

        def getRuleIndex(self):
            return evaluateParser.RULE_number

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNumber" ):
                listener.enterNumber(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNumber" ):
                listener.exitNumber(self)




    def number(self):

        localctx = evaluateParser.NumberContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_number)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 47
            self.match(evaluateParser.NUMBER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





