import pandas as pd
from pandas.api.types import is_numeric_dtype


def arithmetic(api, input_map, operation_func):
    try:
        #CHECK INPUT MESSAGE
        for key in input_map:
            if isinstance(input_map[key].body, Exception):
                api.send("output", input_map[key])
                return
            elif isinstance(input_map[key].body, pd.DataFrame):
                input_map[key] = input_map[key].body
                # rename first/last column to driver/value column and drop driver column
                input_map[key].rename(
                    columns={input_map[key].columns[0]: "driver"}, inplace=True
                )
                input_map[key].drop(columns=["driver"], inplace=True)
                input_map[key].rename(
                    columns={input_map[key].columns[-1]: "value"}, inplace=True
                )
                if not is_numeric_dtype(input_map[key]["value"]):
                    raise Exception(
                        "VALUE COLUMN FROM {} IS NOT NUMERIC TYPE".format(key)
                    )
            elif isinstance(input_map[key].body, float):
                input_map[key] = input_map[key].body
            else:
                raise Exception(
                    'UNKNOWN MESSAGE TYPE "{}" FROM {}'.format(
                        type(input_map[key].body), key
                    )
                )
        output_driver = api.config.output_driver
        if not output_driver:
            output_driver = "TMP"

        # DO OPERATION
        result = operation_func(input_map['in1'], input_map['in2'])

        #ADD DRIVER COLUMN
        if isinstance(result, pd.DataFrame):
            result.insert(0, "driver", output_driver)
        api.send("output", api.Message(result))
    except Exception as e:
        api.logger.info("MY_WARN: {}".format(str(e)))
        api.send("output", api.Message(e))
