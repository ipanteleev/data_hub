grammar evaluate;

query
  : out_driver EQUAL expression EOF
  ;

expression
   : term ((PLUS | MINUS) term)*
   ;

term
   : atom ((TIMES | DIV) atom)*
   ;

//signedAtom
//   : (PLUS|MINUS)? atom
//   ;

atom
   : number
   | variable
   | LPAREN expression RPAREN
   ;

variable
   : VARIABLE
   ;

VARIABLE
   : VALID_ID_START (VALID_ID_CHAR)*
   ;


fragment VALID_ID_START
   : 'in' ('1'..'9') ('0'..'9')* '_'
   ;

fragment VALID_ID_CHAR
   : ('а'..'я')
   | ('a'..'z')
   | ('A'..'Z')
   | ('А'..'Я')
   | ('0' .. '9')
   | '_'
   ;

out_driver
   : OUT_DRIVER
   ;

OUT_DRIVER
   : OUT_LETTER (OUT_LETTER|OUT_DIGIT)+
   ;

fragment OUT_LETTER
   : ('a' .. 'z') | ('A' .. 'Z') | ('а'..'я') | ('А'..'Я') | '_'
;

fragment OUT_DIGIT
   : ('0' .. '9')
;

number
   : NUMBER
   ;


//The integer part gets its potential sign from the signedAtom rule

NUMBER
   : ('0' .. '9') + (('.'|',') ('0' .. '9') +)?
   ;


LPAREN
   : '('
   ;


RPAREN
   : ')'
   ;


PLUS
   : '+'
   ;


MINUS
   : '-'
   ;


TIMES
   : '*'
   ;


DIV
   : '/'
   ;

EQUAL
   : '='
   ;

POINT
   : '.'
   ;

WS
   : [ \r\n\t] + -> skip
;