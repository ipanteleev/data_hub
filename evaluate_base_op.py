from antlr4 import *
import re
import pandas as pd
from pandas.api.types import is_numeric_dtype

from copy import deepcopy

from evaluate.evaluateLexer import evaluateLexer
from evaluate.evaluateParser import evaluateParser
from evaluate.evaluateListener import evaluateListener
import evaluate_operators


op_map = {
    "*": evaluate_operators.mul,
    "/": evaluate_operators.div,
    "+": evaluate_operators.add,
    "-": evaluate_operators.sub,
}


variable_map = {}


class Getter:
    def __init__(self, table, table_name, driver_name):
        self.table_name = table_name
        self.driver = driver_name
        self.table = table[table["driver"] == driver_name]
        self.table.drop(columns=["driver"], inplace=True)

    def aggregate(self, common_columns, agg_method):
        self.table[common_columns] = self.table[common_columns].fillna("")
        self.table = self.table[common_columns + ["value"]]
        if agg_method == "sum":
            self.table = self.table.groupby(common_columns, as_index=False).sum()
        if agg_method == "avg":
            self.table = self.table.groupby(common_columns, as_index=False).mean()


class Listener(evaluateListener):
    def __init__(self):
        super().__init__()
        self.operator_stack = []

        self.additive_op = []
        self.addend_count = 0
        self.storage_add_counter = []

        self.multiplicative = []
        self.multiplier_count = 0
        self.storage_mul_counter = []

        self.out_name = ""

    def exitOut_driver(self, ctx: evaluateParser.Out_driverContext):
        self.out_name = ctx.getTokens(evaluateParser.OUT_DRIVER)[0].getText()

    def enterExpression(self, ctx: evaluateParser.ExpressionContext):
        # check if it is sub expression and store parent params
        if self.additive_op:
            self.storage_add_counter.append(self.addend_count)
            self.addend_count = 0

        # read new operators
        for token in reversed(list(ctx.getChildren())):
            if token.getText() in ["+", "-"]:
                self.additive_op.append(token.getText())

    def exitExpression(self, ctx: evaluateParser.ExpressionContext):
        # end of additive group. Check storage and restore parent expression
        if self.storage_add_counter:
            self.addend_count = self.storage_add_counter.pop()
        else:
            self.addend_count = 0

    def enterTerm(self, ctx: evaluateParser.TermContext):
        # check if it is sub term and store parent params
        if self.multiplicative:
            self.storage_mul_counter.append(self.multiplier_count)
            self.multiplier_count = 0

        # read new operators
        for token in reversed(list(ctx.getChildren())):
            if token.getText() in ["*", "/"]:
                self.multiplicative.append(token.getText())

    def exitTerm(self, ctx: evaluateParser.TermContext):
        # push add operator
        self.addend_count += 1
        if self.addend_count >= 2:
            self.operator_stack.append(self.additive_op.pop())

        # end of multiplicative group. Check storage and restore parent expression
        if self.storage_mul_counter:
            self.multiplier_count = self.storage_mul_counter.pop()
        else:
            self.multiplier_count = 0

    def exitAtom(self, ctx: evaluateParser.AtomContext):
        # push mul operator
        self.multiplier_count += 1
        if self.multiplier_count >= 2:
            self.operator_stack.append(self.multiplicative.pop())

    def enterVariable(self, ctx: evaluateParser.VariableContext):
        text = ctx.getTokens(evaluateParser.VARIABLE)[0].getText()
        self.operator_stack.append(text)

    def enterNumber(self, ctx: evaluateParser.NumberContext):
        num = float(ctx.getTokens(evaluateParser.NUMBER)[0].getText().replace(",", "."))
        self.operator_stack.append(num)


class Evaluate:
    def __init__(self, stack):
        self.stack = stack

    def run(self):
        stack_calc = []
        stack = deepcopy(self.stack)
        while stack:
            var = stack.pop()
            if var in op_map:
                b = stack_calc.pop()
                a = stack_calc.pop()
                stack_calc.append(op_map[var](a, b))
            elif isinstance(var, float):
                stack_calc.append(var)
            else:
                stack_calc.append(variable_map[var].table)
        return stack_calc[0]


def check_formula(formula):
    lexer = evaluateLexer(InputStream(formula))
    token_stream = CommonTokenStream(lexer)
    parser = evaluateParser(token_stream)
    tree = parser.query()
    return True if not parser.getNumberOfSyntaxErrors() else False


def interpret_formula(formula):
    lexer = evaluateLexer(InputStream(formula))
    token_stream = CommonTokenStream(lexer)
    parser = evaluateParser(token_stream)
    tree = parser.query()
    listener = Listener()
    walker = ParseTreeWalker()
    walker.walk(listener, tree)
    return Evaluate(list(reversed(listener.operator_stack))), listener.out_name


def calculate(api, input_map):
    try:
        for key in input_map:
            if isinstance(input_map[key].body, Exception):
                api.send("output", input_map[key])
                return
            elif isinstance(input_map[key].body, pd.DataFrame):
                input_map[key] = input_map[key].body
                # rename first/last column to driver/value column
                input_map[key].rename(
                    columns={input_map[key].columns[0]: "driver"}, inplace=True
                )
                input_map[key].rename(
                    columns={input_map[key].columns[-1]: "value"}, inplace=True
                )
                if not is_numeric_dtype(input_map[key]["value"]):
                    raise Exception(
                        "VALUE COLUMN FROM {} IS NOT NUMERIC TYPE".format(key)
                    )
            else:
                raise Exception(
                    'UNKNOWN MESSAGE TYPE "{}" FROM {}'.format(
                        type(input_map[key]), key
                    )
                )

        formula = api.config.formula

        # check_columns
        for match in re.finditer(r"in[1-9][0-9]*_[А-ЯA-Zа-яa-z][А-ЯA-Zа-яa-z0-9]*", formula):
            print(match.group())
            table, column = match.group().split("_", maxsplit=1)
            if column in input_map[table]["driver"].values:
                variable_map[match.group()] = Getter(input_map[table], table, column)
            else:
                raise Exception("NO DRIVER {} ON INPUT {}".format(column, table))
        if not variable_map:
            raise Exception("NO DRIVER IN FORMULA")

        # do aggr
        if api.config.aggr not in [True, False]:
            raise Exception("INVALID VALUE FOR AGGR PARAM. USE true OR false")
        if api.config.aggr:
            # find common columns
            random_in = list(input_map).pop()
            common_columns = input_map[random_in].columns.values[1:-1]
            for in_df in input_map:
                if in_df == random_in:
                    continue
                common_columns = [
                    col
                    for col in common_columns
                    if col in input_map[in_df].columns.values[1:-1]
                ]
            if not common_columns:
                raise Exception("CAN'T AGGREGATE. NO COMMON COLUMNS")
            for var in variable_map:
                # HERE take aggregation method. Use var.driver. Method: ("sum", "avg")
                agg_method = "sum"
                variable_map[var].aggregate(common_columns, agg_method)

        # interpret formula
        evaluate, output_driver = interpret_formula(formula)

        # calculate new df
        result = evaluate.run()
        result.insert(0, "driver", output_driver)

        # API MESSAGE
        api.send("output", api.Message(result))
    except Exception as e:
        api.logger.info("MY_WARN: {}".format(str(e)))
        api.send("output", api.Message(e))


# def log_warning(api, text):
#   api.logger.info("MY_WARN: {}".format(str(e)))
