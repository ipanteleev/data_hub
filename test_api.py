class api:
    class config:
        # ADD NEW FIELD IF CREATE NEW OPERATORS

        # evaluate
        formula = ""
        aggr = False

        # filter
        filter = ""

        # aggregate
        columns = ""
        aggregate_method = "sum"

        # add, sub, div, mul
        output_driver = ""

        # constant
        constant_value = ""

    class Message:
        def __init__(self, data, attribute=None):
            if attribute is None:
                attribute = {}
            self.body = data
            self.attribute = attribute

    class logger:
        @staticmethod
        def info(text):
            print(text)

    @staticmethod
    def send(tag, mess):
        print("MESSAGE:")
        print(mess.body)

    @staticmethod
    def propagate_exception(e):
        print("EXCEPTION: {}".format(str(e)))

    @staticmethod
    def add_generator(on_generate):
        print("SET GENERATOR METHOD")

    @staticmethod
    def set_port_callback(input_array, callback):
        print("SET PORT CALLBACK")
