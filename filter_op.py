from antlr4 import *
import pandas as pd

import operator
from copy import deepcopy


import sys

sys.path.append("/app/data_hub")

from filter_.filter_Lexer import filter_Lexer
from filter_.filter_Parser import filter_Parser
from filter_.filter_Listener import filter_Listener

try:
    api
except:
    from test_api import api

op_map = {"&": operator.and_, "|": operator.or_}


class Listener(filter_Listener):
    def __init__(self):
        self.or_counter = 0
        self.storage_or_counter = []

        self.and_counter = 0
        self.storage_and_counter = []

        self.operators = []
        self.operator = {}
        self.cur_col = ""

        self.result = []

    def enterOr_expr(self, ctx: filter_Parser.Or_exprContext):
        self.or_counter = 0

    def enterAnd_expr(self, ctx: filter_Parser.And_exprContext):
        self.and_counter = 0

    def exitAnd_expr(self, ctx: filter_Parser.And_exprContext):
        self.or_counter += 1
        if self.or_counter >= 2:
            self.result.append("|")

    def enterTerm(self, ctx: filter_Parser.TermContext):
        sub_expr = ctx.getToken(filter_Parser.LPAREN, i=0)
        if sub_expr:
            self.storage_or_counter.append(self.or_counter)
            self.storage_and_counter.append(self.and_counter)

    def exitTerm(self, ctx: filter_Parser.TermContext):
        sub_expr = ctx.getToken(filter_Parser.RPAREN, i=0)
        if sub_expr:
            self.or_counter = self.storage_or_counter.pop()
            self.and_counter = self.storage_and_counter.pop()

        self.and_counter += 1
        if self.and_counter >= 2:
            self.result.append("&")

    def enterEqual_expr(self, ctx: filter_Parser.Equal_exprContext):
        pass

    def exitEqual_expr(self, ctx: filter_Parser.Equal_exprContext):
        self.result.append(self.cur_col)
        self.operators.append(self.operator)
        self.operator = {}

    def enterColumn(self, ctx: filter_Parser.ColumnContext):
        col = ctx.getToken(ttype=filter_Parser.WORD, i=0)
        self.operator[col.getText()] = []
        self.cur_col = col.getText()

    def enterOperator(self, ctx: filter_Parser.OperatorContext):
        operators = ctx.getTokens(ttype=filter_Parser.WORD)
        self.operator[self.cur_col].append(" ".join([op.getText() for op in operators]))


class Evaluate:
    def __init__(self, stack, operators):
        self.stack = stack
        self.operators = operators

    def run(self, df):
        stack_calc = []
        stack = deepcopy(self.stack)
        while stack:
            var = stack.pop()
            if var in op_map:
                b = stack_calc.pop()
                a = stack_calc.pop()
                stack_calc.append(op_map[var](a, b))
            else:
                restriction = self.operators.pop()
                key = list(restriction)[0]
                mask = False
                for r in restriction[key]:
                    mask = mask | (df[key] == r)
                stack_calc.append(mask)
        return stack_calc[0]


def interpret_formula(filter_str):
    lexer = filter_Lexer(InputStream(filter_str))
    token_stream = CommonTokenStream(lexer)
    parser = filter_Parser(token_stream)
    tree = parser.or_expr()
    listener = Listener()
    walker = ParseTreeWalker()
    walker.walk(listener, tree)
    return Evaluate(list(reversed(listener.result)), list(reversed(listener.operators)))


def is_formula_correct(filter_str):
    lexer = filter_Lexer(InputStream(filter_str))
    token_stream = CommonTokenStream(lexer)
    parser = filter_Parser(token_stream)
    tree = parser.query()
    return True if not parser.getNumberOfSyntaxErrors() else False


def on_input(in1):
    try:
        # if receive message
        if isinstance(in1.body, Exception):
            api.send("output", in1)
            return
        elif isinstance(in1.body, pd.DataFrame):
            in1 = in1.body
        else:
            raise Exception('UNKNOWN MESSAGE TYPE "{}"'.format(type(in1.body)))

        mask_evaluator = interpret_formula(api.config.filter)

        # check columns in df
        for op_dict in mask_evaluator.operators:
            col_name = list(op_dict)[0]
            if not col_name in in1:
                raise Exception("NO COLUMN {} ON INPUT".format(col_name))

        in1 = in1[mask_evaluator.run(in1)]

        # send result
        api.send("output", api.Message(in1))
    except Exception as e:
        api.logger.info("MY_WARN: {}".format(str(e)))
        api.send("output", api.Message(e))


def on_generate():
    # check is formula correct
    if not is_formula_correct(api.config.filter):
        api.send(
            "output",
            api.Message(
                Exception("Error in filter formula: {}".format(api.config.filter))
            ),
        )
        api.propagate_exception(
            Exception("Error in filter formula: {}".format(api.config.filter))
        )


# def log_warning(text):
#     api.logger.info("MY_WARN: {}".format(text))


api.add_generator(on_generate)
api.set_port_callback("in1", on_input)
