import sys

sys.path.append("/app/data_hub")

from evaluate_operators import mul
from arithmetic_base_op import arithmetic

try:
    api
except:
    from test_api import api


def on_input(in1, in2):
    # CHECK IF INPUT IS MESSAGE TYPE
    input_map = {"in1": in1, "in2": in2}
    arithmetic(api, input_map, mul)


api.set_port_callback(["in1", "in2"], on_input)
