# generate_method
pass

# callback_method
from aggregate_op import on_input

import pandas as pd

data1 = pd.DataFrame(
    {
        "driver": ["dr", "dr", "dr"],
        "attr": ["a1", "a1", "a3"],
        "b": ["b1", None, "b3"],
        "value": [10, 20, 30],
    }
)

columns = "attr"
aggregation_method = "sum"

from test_api import api

api.config.columns = columns
api.config.aggregate_method = aggregation_method


in1 = api.Message(data1)

on_input(in1)
