# generate_method
pass

# callback_method
from sub_op import on_input

import pandas as pd

data1 = pd.DataFrame(
    {
        "driver": ["dr", "dr", "dr"],
        "attr": ["a1", "a1", "a3"],
        "b": ["b1", None, "b3"],
        "value": [10, 20, 30],
    }
)
data2 = pd.DataFrame({"driver": ["mr"], "attr": ["a1"], "value": [10]})
data1 = 123.0
# data2 = 0.0
from test_api import api

in1 = api.Message(data1)
in2 = api.Message(data2)
on_input(in1, in2)
