# generate_method
from filter_op import on_generate

# callback_method
from filter_op import on_input

import pandas as pd

data1 = pd.DataFrame(
    {
        "driver": ["dr", "dr", "dr2"],
        "attr": ["a1", "a1", "a3"],
        "b": ["b1", None, "b3"],
        "value": [10, 20, 30],
    }
)

filter_ = "driver=dr"

from test_api import api

api.config.filter = filter_

on_generate()

in1 = api.Message(data1)

on_input(in1)
