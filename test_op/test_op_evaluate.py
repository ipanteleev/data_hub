# generate_method
from evaluate_2in_op import on_generate

# callback_method
from evaluate_2in_op import on_input

import pandas as pd

data1 = pd.DataFrame(
    {
        "driver": ["dr", "dr", "dr"],
        "attr": ["a1", "a1", "a3"],
        "b": ["b1", None, "b3"],
        "value": [10, 20, 30],
    }
)
data2 = pd.DataFrame({"driver": ["mr"], "attr": ["a1"], "value": [10]})
formula = "RES31 = in1_dr * in2_mr"
aggr = True

from test_api import api

api.config.formula = formula
api.config.aggr = aggr
on_generate()

in1 = api.Message(data1)
in2 = api.Message(data2)
on_input(in1, in2)
