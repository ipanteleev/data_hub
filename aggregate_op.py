import pandas as pd

try:
    api
except:
    from test_api import api


def on_input(in1):
    try:
        # if receive message
        if isinstance(in1.body, Exception):
            api.send("output", in1)
            return
        elif isinstance(in1.body, pd.DataFrame):
            in1 = in1.body
        else:
            raise Exception('UNKNOWN MESSAGE TYPE "{}"'.format(type(in1.body)))

        has_driver_column = False
        driver_column = in1.columns.values[0]
        value_column = in1.columns.values[-1]
        columns = api.config.columns.split(",")
        # strip
        columns = [c.strip(" ") for c in columns]
        aggregate_method = api.config.aggregate_method

        for c in columns:
            if c == driver_column:
                has_driver_column = True
            if c == value_column:
                raise Exception("CAN'T AGGREGATE BY {} VALUE COLUMN".format(c))
            if not c in in1:
                raise Exception("NO COLUMN {} ON INPUT".format(c if c else "None"))

        if not aggregate_method in ["sum", "avg"]:
            raise Exception(
                "INVALID AGGR METHOD {}. USE sum OR avg.".format(aggregate_method)
            )

        # delete driver column, and insert it first
        if has_driver_column:
            columns.remove(driver_column)
        columns.insert(0, driver_column)

        # project
        in1[columns] = in1[columns].fillna("")
        in1 = in1[columns + [value_column]]

        if aggregate_method == "sum":
            in1 = in1.groupby(columns, as_index=False).sum()
        elif aggregate_method == "avg":
            in1 = in1.groupby(columns, as_index=False).mean()

        # send result
        api.send("output", api.Message(in1))
    except Exception as e:
        api.logger.info("MY_WARN: {}".format(str(e)))
        api.send("output", api.Message(e))


# def log_warning(text):
#     api.logger.info("MY_WARN: {}".format(text))


api.set_port_callback("in1", on_input)
