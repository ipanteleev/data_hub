import sys

sys.path.append("/app/data_hub")

from evaluate_base_op import check_formula, calculate

try:
    api
except:
    from test_api import api


def on_input(in1, in2, in3):
    # CHECK IF INPUT IS MESSAGE TYPE
    input_map = {"in1": in1, "in2": in2, "in3": in3}
    calculate(api, input_map)


def on_generate():
    if not check_formula(api.config.formula):
        # log_warning("Error in formula: {}".format(api.config.formula))
        api.send(
            "output",
            api.Message(Exception("Error in formula: {}".format(api.config.formula))),
        )
        api.propagate_exception(
            Exception("Error in formula: {}".format(api.config.formula))
        )


api.add_generator(on_generate)
api.set_port_callback(["in1", "in2", "in3"], on_input)
