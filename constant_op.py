import sys

sys.path.append("/app/data_hub")

from evaluate_operators import add
from arithmetic_base_op import arithmetic

try:
    api
except:
    from test_api import api


def on_generate():
    # CHECK IF INPUT IS MESSAGE TYPE
    try:
        constant_str = float(str(api.config.constant_value).replace(",", "."))
        api.send("output", api.Message(constant_str))
    except Exception as e:
        api.send(
            "output",
            api.Message(
                Exception("CONSTANT {} NOT A FLOAT".format(api.config.constant_value))
            ),
        )
        api.propagate_exception(
            Exception("CONSTANT {} NOT A FLOAT".format(api.config.constant_value))
        )


api.add_generator(on_generate)
