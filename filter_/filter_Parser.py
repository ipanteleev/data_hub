# Generated from C:/Users/ipanteleev/PycharmProjects/customOperators\filter_.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\f")
        buf.write("H\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\3\2\3\2\3\2\3\3\3\3\3\3\7\3\27\n\3\f\3\16\3\32\13")
        buf.write("\3\3\4\3\4\3\4\7\4\37\n\4\f\4\16\4\"\13\4\3\5\3\5\3\5")
        buf.write("\3\5\3\5\5\5)\n\5\3\6\3\6\3\6\3\6\3\6\7\6\60\n\6\f\6\16")
        buf.write("\6\63\13\6\3\6\5\6\66\n\6\3\7\3\7\3\b\3\b\6\b<\n\b\r\b")
        buf.write("\16\b=\3\b\3\b\6\bB\n\b\r\b\16\bC\5\bF\n\b\3\b\2\2\t\2")
        buf.write("\4\6\b\n\f\16\2\2\2H\2\20\3\2\2\2\4\23\3\2\2\2\6\33\3")
        buf.write("\2\2\2\b(\3\2\2\2\n*\3\2\2\2\f\67\3\2\2\2\16E\3\2\2\2")
        buf.write("\20\21\5\4\3\2\21\22\7\2\2\3\22\3\3\2\2\2\23\30\5\6\4")
        buf.write("\2\24\25\7\6\2\2\25\27\5\6\4\2\26\24\3\2\2\2\27\32\3\2")
        buf.write("\2\2\30\26\3\2\2\2\30\31\3\2\2\2\31\5\3\2\2\2\32\30\3")
        buf.write("\2\2\2\33 \5\b\5\2\34\35\7\5\2\2\35\37\5\b\5\2\36\34\3")
        buf.write("\2\2\2\37\"\3\2\2\2 \36\3\2\2\2 !\3\2\2\2!\7\3\2\2\2\"")
        buf.write(" \3\2\2\2#)\5\n\6\2$%\7\3\2\2%&\5\4\3\2&\'\7\4\2\2\')")
        buf.write("\3\2\2\2(#\3\2\2\2($\3\2\2\2)\t\3\2\2\2*+\5\f\7\2+,\7")
        buf.write("\t\2\2,\61\5\16\b\2-.\7\7\2\2.\60\5\16\b\2/-\3\2\2\2\60")
        buf.write("\63\3\2\2\2\61/\3\2\2\2\61\62\3\2\2\2\62\65\3\2\2\2\63")
        buf.write("\61\3\2\2\2\64\66\7\7\2\2\65\64\3\2\2\2\65\66\3\2\2\2")
        buf.write("\66\13\3\2\2\2\678\7\n\2\28\r\3\2\2\29;\7\b\2\2:<\7\n")
        buf.write("\2\2;:\3\2\2\2<=\3\2\2\2=;\3\2\2\2=>\3\2\2\2>?\3\2\2\2")
        buf.write("?F\7\b\2\2@B\7\n\2\2A@\3\2\2\2BC\3\2\2\2CA\3\2\2\2CD\3")
        buf.write("\2\2\2DF\3\2\2\2E9\3\2\2\2EA\3\2\2\2F\17\3\2\2\2\n\30")
        buf.write(" (\61\65=CE")
        return buf.getvalue()


class filter_Parser ( Parser ):

    grammarFileName = "filter_.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'('", "')'" ]

    symbolicNames = [ "<INVALID>", "LPAREN", "RPAREN", "AND", "OR", "COMMA", 
                      "QUOTE", "EQUAL", "WORD", "LETTER", "WS" ]

    RULE_query = 0
    RULE_or_expr = 1
    RULE_and_expr = 2
    RULE_term = 3
    RULE_equal_expr = 4
    RULE_column = 5
    RULE_operator = 6

    ruleNames =  [ "query", "or_expr", "and_expr", "term", "equal_expr", 
                   "column", "operator" ]

    EOF = Token.EOF
    LPAREN=1
    RPAREN=2
    AND=3
    OR=4
    COMMA=5
    QUOTE=6
    EQUAL=7
    WORD=8
    LETTER=9
    WS=10

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class QueryContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def or_expr(self):
            return self.getTypedRuleContext(filter_Parser.Or_exprContext,0)


        def EOF(self):
            return self.getToken(filter_Parser.EOF, 0)

        def getRuleIndex(self):
            return filter_Parser.RULE_query

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterQuery" ):
                listener.enterQuery(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitQuery" ):
                listener.exitQuery(self)




    def query(self):

        localctx = filter_Parser.QueryContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_query)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 14
            self.or_expr()
            self.state = 15
            self.match(filter_Parser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Or_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def and_expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(filter_Parser.And_exprContext)
            else:
                return self.getTypedRuleContext(filter_Parser.And_exprContext,i)


        def OR(self, i:int=None):
            if i is None:
                return self.getTokens(filter_Parser.OR)
            else:
                return self.getToken(filter_Parser.OR, i)

        def getRuleIndex(self):
            return filter_Parser.RULE_or_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOr_expr" ):
                listener.enterOr_expr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOr_expr" ):
                listener.exitOr_expr(self)




    def or_expr(self):

        localctx = filter_Parser.Or_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_or_expr)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 17
            self.and_expr()
            self.state = 22
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==filter_Parser.OR:
                self.state = 18
                self.match(filter_Parser.OR)
                self.state = 19
                self.and_expr()
                self.state = 24
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class And_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(filter_Parser.TermContext)
            else:
                return self.getTypedRuleContext(filter_Parser.TermContext,i)


        def AND(self, i:int=None):
            if i is None:
                return self.getTokens(filter_Parser.AND)
            else:
                return self.getToken(filter_Parser.AND, i)

        def getRuleIndex(self):
            return filter_Parser.RULE_and_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnd_expr" ):
                listener.enterAnd_expr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnd_expr" ):
                listener.exitAnd_expr(self)




    def and_expr(self):

        localctx = filter_Parser.And_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_and_expr)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 25
            self.term()
            self.state = 30
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==filter_Parser.AND:
                self.state = 26
                self.match(filter_Parser.AND)
                self.state = 27
                self.term()
                self.state = 32
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TermContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def equal_expr(self):
            return self.getTypedRuleContext(filter_Parser.Equal_exprContext,0)


        def LPAREN(self):
            return self.getToken(filter_Parser.LPAREN, 0)

        def or_expr(self):
            return self.getTypedRuleContext(filter_Parser.Or_exprContext,0)


        def RPAREN(self):
            return self.getToken(filter_Parser.RPAREN, 0)

        def getRuleIndex(self):
            return filter_Parser.RULE_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm" ):
                listener.enterTerm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm" ):
                listener.exitTerm(self)




    def term(self):

        localctx = filter_Parser.TermContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_term)
        try:
            self.state = 38
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [filter_Parser.WORD]:
                self.enterOuterAlt(localctx, 1)
                self.state = 33
                self.equal_expr()
                pass
            elif token in [filter_Parser.LPAREN]:
                self.enterOuterAlt(localctx, 2)
                self.state = 34
                self.match(filter_Parser.LPAREN)
                self.state = 35
                self.or_expr()
                self.state = 36
                self.match(filter_Parser.RPAREN)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Equal_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def column(self):
            return self.getTypedRuleContext(filter_Parser.ColumnContext,0)


        def EQUAL(self):
            return self.getToken(filter_Parser.EQUAL, 0)

        def operator(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(filter_Parser.OperatorContext)
            else:
                return self.getTypedRuleContext(filter_Parser.OperatorContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(filter_Parser.COMMA)
            else:
                return self.getToken(filter_Parser.COMMA, i)

        def getRuleIndex(self):
            return filter_Parser.RULE_equal_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEqual_expr" ):
                listener.enterEqual_expr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEqual_expr" ):
                listener.exitEqual_expr(self)




    def equal_expr(self):

        localctx = filter_Parser.Equal_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_equal_expr)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 40
            self.column()
            self.state = 41
            self.match(filter_Parser.EQUAL)
            self.state = 42
            self.operator()
            self.state = 47
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,3,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 43
                    self.match(filter_Parser.COMMA)
                    self.state = 44
                    self.operator() 
                self.state = 49
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,3,self._ctx)

            self.state = 51
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==filter_Parser.COMMA:
                self.state = 50
                self.match(filter_Parser.COMMA)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ColumnContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WORD(self):
            return self.getToken(filter_Parser.WORD, 0)

        def getRuleIndex(self):
            return filter_Parser.RULE_column

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterColumn" ):
                listener.enterColumn(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitColumn" ):
                listener.exitColumn(self)




    def column(self):

        localctx = filter_Parser.ColumnContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_column)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 53
            self.match(filter_Parser.WORD)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class OperatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def QUOTE(self, i:int=None):
            if i is None:
                return self.getTokens(filter_Parser.QUOTE)
            else:
                return self.getToken(filter_Parser.QUOTE, i)

        def WORD(self, i:int=None):
            if i is None:
                return self.getTokens(filter_Parser.WORD)
            else:
                return self.getToken(filter_Parser.WORD, i)

        def getRuleIndex(self):
            return filter_Parser.RULE_operator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOperator" ):
                listener.enterOperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOperator" ):
                listener.exitOperator(self)




    def operator(self):

        localctx = filter_Parser.OperatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_operator)
        self._la = 0 # Token type
        try:
            self.state = 67
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [filter_Parser.QUOTE]:
                self.enterOuterAlt(localctx, 1)
                self.state = 55
                self.match(filter_Parser.QUOTE)
                self.state = 57 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 56
                    self.match(filter_Parser.WORD)
                    self.state = 59 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==filter_Parser.WORD):
                        break

                self.state = 61
                self.match(filter_Parser.QUOTE)
                pass
            elif token in [filter_Parser.WORD]:
                self.enterOuterAlt(localctx, 2)
                self.state = 63 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 62
                    self.match(filter_Parser.WORD)
                    self.state = 65 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==filter_Parser.WORD):
                        break

                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





