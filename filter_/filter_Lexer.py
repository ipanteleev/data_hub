# Generated from C:/Users/ipanteleev/PycharmProjects/customOperators\filter_.g4 by ANTLR 4.7.2
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\f")
        buf.write("E\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\3\2\3\2\3\3\3\3\3\4")
        buf.write("\3\4\3\4\3\4\3\4\3\4\3\4\5\4#\n\4\3\5\3\5\3\5\3\5\3\5")
        buf.write("\3\5\3\5\5\5,\n\5\3\6\3\6\3\7\3\7\3\b\3\b\3\b\5\b\65\n")
        buf.write("\b\3\t\6\t8\n\t\r\t\16\t9\3\n\5\n=\n\n\3\13\6\13@\n\13")
        buf.write("\r\13\16\13A\3\13\3\13\2\2\f\3\3\5\4\7\5\t\6\13\7\r\b")
        buf.write("\17\t\21\n\23\13\25\f\3\2\6\4\2..==\4\2$$))\b\2/\60\62")
        buf.write(";C\\aac|\u0412\u0451\5\2\13\f\17\17\"\"\2L\2\3\3\2\2\2")
        buf.write("\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r")
        buf.write("\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3")
        buf.write("\2\2\2\3\27\3\2\2\2\5\31\3\2\2\2\7\"\3\2\2\2\t+\3\2\2")
        buf.write("\2\13-\3\2\2\2\r/\3\2\2\2\17\64\3\2\2\2\21\67\3\2\2\2")
        buf.write("\23<\3\2\2\2\25?\3\2\2\2\27\30\7*\2\2\30\4\3\2\2\2\31")
        buf.write("\32\7+\2\2\32\6\3\2\2\2\33\34\7C\2\2\34\35\7P\2\2\35#")
        buf.write("\7F\2\2\36\37\7c\2\2\37 \7p\2\2 #\7f\2\2!#\7(\2\2\"\33")
        buf.write("\3\2\2\2\"\36\3\2\2\2\"!\3\2\2\2#\b\3\2\2\2$%\7Q\2\2%")
        buf.write(",\7T\2\2&\'\7q\2\2\',\7t\2\2(,\7~\2\2)*\7~\2\2*,\7~\2")
        buf.write("\2+$\3\2\2\2+&\3\2\2\2+(\3\2\2\2+)\3\2\2\2,\n\3\2\2\2")
        buf.write("-.\t\2\2\2.\f\3\2\2\2/\60\t\3\2\2\60\16\3\2\2\2\61\65")
        buf.write("\7?\2\2\62\63\7?\2\2\63\65\7?\2\2\64\61\3\2\2\2\64\62")
        buf.write("\3\2\2\2\65\20\3\2\2\2\668\5\23\n\2\67\66\3\2\2\289\3")
        buf.write("\2\2\29\67\3\2\2\29:\3\2\2\2:\22\3\2\2\2;=\t\4\2\2<;\3")
        buf.write("\2\2\2=\24\3\2\2\2>@\t\5\2\2?>\3\2\2\2@A\3\2\2\2A?\3\2")
        buf.write("\2\2AB\3\2\2\2BC\3\2\2\2CD\b\13\2\2D\26\3\2\2\2\t\2\"")
        buf.write("+\649<A\3\b\2\2")
        return buf.getvalue()


class filter_Lexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    LPAREN = 1
    RPAREN = 2
    AND = 3
    OR = 4
    COMMA = 5
    QUOTE = 6
    EQUAL = 7
    WORD = 8
    LETTER = 9
    WS = 10

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'('", "')'" ]

    symbolicNames = [ "<INVALID>",
            "LPAREN", "RPAREN", "AND", "OR", "COMMA", "QUOTE", "EQUAL", 
            "WORD", "LETTER", "WS" ]

    ruleNames = [ "LPAREN", "RPAREN", "AND", "OR", "COMMA", "QUOTE", "EQUAL", 
                  "WORD", "LETTER", "WS" ]

    grammarFileName = "filter_.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


