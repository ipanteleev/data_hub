# Generated from C:/Users/ipanteleev/PycharmProjects/customOperators\filter_.g4 by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .filter_Parser import filter_Parser
else:
    from filter_Parser import filter_Parser

# This class defines a complete listener for a parse tree produced by filter_Parser.
class filter_Listener(ParseTreeListener):

    # Enter a parse tree produced by filter_Parser#query.
    def enterQuery(self, ctx:filter_Parser.QueryContext):
        pass

    # Exit a parse tree produced by filter_Parser#query.
    def exitQuery(self, ctx:filter_Parser.QueryContext):
        pass


    # Enter a parse tree produced by filter_Parser#or_expr.
    def enterOr_expr(self, ctx:filter_Parser.Or_exprContext):
        pass

    # Exit a parse tree produced by filter_Parser#or_expr.
    def exitOr_expr(self, ctx:filter_Parser.Or_exprContext):
        pass


    # Enter a parse tree produced by filter_Parser#and_expr.
    def enterAnd_expr(self, ctx:filter_Parser.And_exprContext):
        pass

    # Exit a parse tree produced by filter_Parser#and_expr.
    def exitAnd_expr(self, ctx:filter_Parser.And_exprContext):
        pass


    # Enter a parse tree produced by filter_Parser#term.
    def enterTerm(self, ctx:filter_Parser.TermContext):
        pass

    # Exit a parse tree produced by filter_Parser#term.
    def exitTerm(self, ctx:filter_Parser.TermContext):
        pass


    # Enter a parse tree produced by filter_Parser#equal_expr.
    def enterEqual_expr(self, ctx:filter_Parser.Equal_exprContext):
        pass

    # Exit a parse tree produced by filter_Parser#equal_expr.
    def exitEqual_expr(self, ctx:filter_Parser.Equal_exprContext):
        pass


    # Enter a parse tree produced by filter_Parser#column.
    def enterColumn(self, ctx:filter_Parser.ColumnContext):
        pass

    # Exit a parse tree produced by filter_Parser#column.
    def exitColumn(self, ctx:filter_Parser.ColumnContext):
        pass


    # Enter a parse tree produced by filter_Parser#operator.
    def enterOperator(self, ctx:filter_Parser.OperatorContext):
        pass

    # Exit a parse tree produced by filter_Parser#operator.
    def exitOperator(self, ctx:filter_Parser.OperatorContext):
        pass


